from instapy import InstaPy

import random

def shuffle(array):
    return random.sample( array, len(array) )

def random_int(min, max):
    return random.randint(min, max)

# app
session = InstaPy(username='username', password='password', nogui=True)

# login
session.login()

# vars
# people_to_follow = ['therock','natgeo']
hashtags = ['#miami', '#miamibusiness', '#miamibusinesses','#website', '#websitedesign', '#marketing', '#miamiagency','#realestatemiami']

# options
# session.set_do_comment(False, percentage=0)
session.set_do_follow(enabled=True, percentage=50, times=2)
# session.set_lower_follower_count(limit = 200)
# session.set_upper_follower_count(limit = 250)
# session.set_dont_like(['#gay', '#selfie'])
# session.set_ignore_if_contains(['glutenfree', 'french', 'tasty'])
# session.follow_by_list(people_to_follow, times=1)
# session.set_ignore_users(['random_user', 'another_username'])
# session.set_dont_include(['friend2', 'friend3'])

# preferences
session.like_by_tags(shuffle(hashtags), amount=random_int(10,20), media='Photo')
session.like_by_feed(amount=random_int(10,50))
# session.like_from_image(amount=100)
# session.like_by_locations(['224442573/salton-sea/'], amount=100)

# end
session.end()